#/bin/bash
{
  pkexec bash $(dirname "$(readlink -f "$0")")/library-settings.sh Install
  yad --width=300 --title=CCRLD --on-top --center --text-align=center --text "<b><big>Application is installed</big></b>" --button=OK:1
} > /dev/null 2>&1
