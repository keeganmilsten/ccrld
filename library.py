# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
import time
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("CCRLD")
try:
    import httplib
except:
    import http.client as httplib

workingDirectory = os.path.dirname(os.path.realpath(__file__))
os.system('eopkg history | grep "Operation #" | grep -E "upgrade|install|takeback" | head -n 15 | sed s/takeback/rollback/g >' + workingDirectory + '/updates.txt')
os.system('bash ' + workingDirectory + '/library-settings.sh SearchUpdates')
os.system('bash ' + workingDirectory + '/library-settings.sh SyncStatus ' + workingDirectory)

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. Some options may not work").show()

class Handler:
    def __init__(self):
        self.listoptions1 = [0,0,0,0,0,0,0,0]

# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### First Tab ######################################
################################################################################

# Launch Solus' Update tab
    def clickUpdate(self, button):
        os.system('solus-sc --update-view')

################################################################################
############################### Second Tab ######################################
################################################################################

# Save Program List
    def onFileChosen(self, menuitem):
        global filechosen
        filechosen = menuitem.get_file().get_path()
        print("selected wallpaper: ", filechosen)
        print()

# Save To
    def onWallpaper(self, button):
        os.system('pkexec bash ' + workingDirectory + '/library-settings.sh Wallpapers "' + filechosen + '"')

################################################################################
############################### Third Tab ######################################
################################################################################

    def onRestrictSettings(self, switch, state):
        if state == True:
            self.listoptions1[0]=1
        else:
            self.listoptions1[0]=0

    def onDeRestricSettings(self, switch, state):
        if state == True:
            self.listoptions1[1]=1
        else:
            self.listoptions1[1]=0

    def onFixLibreOffice(self, switch, state):
        if state == True:
            self.listoptions1[2]=1
        else:
            self.listoptions1[2]=0

    def onConfigurePrinters(self, switch, state):
        if state == True:
            self.listoptions1[3]=1
        else:
            self.listoptions1[3]=0

    def onFirefoxSettings(self, switch, state):
        if state == True:
            self.listoptions1[4]=1
        else:
            self.listoptions1[4]=0

    def onPowerStrip(self, switch, state):
        if state == True:
            self.listoptions1[5]=1
        else:
            self.listoptions1[5]=0

    def onDesktopIcons(self, switch, state):
        if state == True:
            self.listoptions1[6]=1
        else:
            self.listoptions1[6]=0

    def onMasterFix(self, switch, state):
        if state == True:
            self.listoptions1[7]=1
        else:
            self.listoptions1[7]=0

    def clickApply1(self, button):
        print(self.listoptions1)
        if self.listoptions1[0] == 1:
            print("Restricting Settings for Patrons...")
            os.system('pkexec bash ' + workingDirectory + '/library-settings.sh Restrict')
            Notify.Notification.new("Restricted Settings").show()

        if self.listoptions1[1] == 1:
            print("Unrestricting Settings for Patrons...")
            os.system('pkexec bash ' + workingDirectory + '/library-settings.sh De_Restrict')
            Notify.Notification.new("Settings Unrestricted").show()

        if self.listoptions1[2] == 1:
            os.system('bash ' + workingDirectory + '/library-settings.sh LibreOffice')

        if self.listoptions1[3] == 1:
            os.system('bash ' + workingDirectory + '/library-settings.sh Printers')

        if self.listoptions1[4] == 1:
            Notify.Notification.new("Fixing Firefox Settings...").show()
            os.system('pkexec bash ' + workingDirectory + '/library-settings.sh Firefox')
            Notify.Notification.new("Firefox Settings Fixed").show()

        if self.listoptions1[5] == 1:
            os.system('pkexec bash ' + workingDirectory + '/library-settings.sh Power_Strip')
            Notify.Notification.new("Power Strip is now present").show()

        if self.listoptions1[6] == 1:
            os.system('pkexec bash ' + workingDirectory + '/library-settings.sh DesktopIcons')
            Notify.Notification.new("State of Desktop Icons Changed").show()

        if self.listoptions1[7] == 1:
            Notify.Notification.new("Running Master Fix...").show()
            os.system('pkexec bash ' + workingDirectory + '/library-settings.sh Maintenance')
            Notify.Notification.new("Master Fixed").show()

################################################################################
############################### Fourth Tab #####################################
################################################################################

    # Enable Sync
    def onSyncEnable(self, button):
        os.system('pkexec bash ' + workingDirectory + '/sync/sync.sh Enable')
        os.system('yad --selectable-labels --text "PLEASE ADD THE FOLLOWING TEXT AT THE END OF THE FILE THAT SHALL OPEN (IF NOT ALREADY PRESENT):\n     ALL ALL=(ALL) NOPASSWD: /sync/.patron-sync/sync.sh"')
        os.system('gedit admin:///etc/sudoers')
        Notify.Notification.new("Sync Enabled").show()

    # Disable Sync
    def onSyncDisable(self, button):
        os.system('pkexec bash ' + workingDirectory + '/sync/sync.sh Disable')
        Notify.Notification.new("Sync Disabled").show()

    # Open Patron home directory as root
    def onPatronFolder(self, button):
        os.system('nautilus admin:///home/patron')

################################################################################
############################### Fifth Tab ######################################
################################################################################

    # Execute a rollback
    def onOperationEntry(self, pkgtxt):
        global enteredText
        enteredText = pkgtxt.get_text()
        textMinusOne = int(enteredText) - 1
        finalText = str(textMinusOne)
        os.system('pkexec eopkg -y history -t ' + finalText)
        Notify.Notification.new("Rollback Complete").show()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/library.glade")
builder.connect_signals(Handler())

# =====================================
# View the file when the window pops up
# =====================================
with open(workingDirectory + '/updates.txt', 'r') as pacFile:
    data = pacFile.read()
    builder.get_object("fileContentsView").get_buffer().set_text(data)
# =====================================

# =====================================
# View the file when the window pops up
# =====================================
with open(workingDirectory + '/sync_status.txt', 'r') as pacFile2:
    data = pacFile2.read()
    builder.get_object("fileContentsView2").get_buffer().set_text(data)
# =====================================

window2 = builder.get_object("Reborn")
window2.show_all()

Gtk.main()

Notify.uninit()
