#/bin/bash
###############################################################################################
####### Restrict and Allow the following options in GNOME Settings to Staff + Root only #######
###############################################################################################

# SOLUS HISTORY: sudo eopkg history | grep "Operation #" | grep "upgrade" | head -n 10
function Restrict() {
        echo
	# Background Picture
	if [ -f "/usr/share/applications/gnome-background-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-background-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-background-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Background Picture Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Background Picture Settings\033[0m"
	fi

	# Date + Time
	if [ -f "/usr/share/applications/gnome-datetime-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-datetime-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-datetime-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Date + Time Settings"
		else
			echo -e "\033[0;31m\u274c Failed to Restrict Date + Time Settings\033[0m"
	fi

	# Default Apps
	if [ -f "/usr/share/applications/gnome-default-apps-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-default-apps-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-default-apps-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Default App Settings"
		else
			echo -e "\033[0;31m\u274c Failed to Restrict Default App Settings\033[0m"
	fi

	# Display Configurations
	if [ -f "/usr/share/applications/gnome-display-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-display-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-display-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Display Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Display Settings\033[0m"
	fi

	# Keyboard Configurations
	if [ -f "/usr/share/applications/gnome-keyboard-panel.desktop" ]; then
	sudo chown staff:staff /usr/share/applications/gnome-keyboard-panel.desktop
	sudo chmod 700 /usr/share/applications/gnome-keyboard-panel.desktop
	sudo chown staff:staff /usr/share/applications/gkbd-keyboard-display.desktop
	sudo chmod 700 /usr/share/applications/gkbd-keyboard-display.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Keyboard Settings"
		else
			echo -e "\033[0;31m\u274c Failed to Restrict Keyboard Settings\033[0m"
	fi

	# Mouse Configurations
	if [ -f "/usr/share/applications/gnome-mouse-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-mouse-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-mouse-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Mouse Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Mouse Settings\033[0m"
	fi

	# Notifications
	if [ -f "/usr/share/applications/gnome-notifications-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-notifications-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-notifications-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Notification Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Notification Settings\033[0m"
	fi

	# Online Account Integration
	echo -e "\033[1;32m\xE2\x9C\x94 Restricted Online Account Integration Settings"
	if [ -f "/usr/share/applications/gnome-online-accounts-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-online-accounts-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-online-accounts-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Online Account Integration Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Online Account Settings\033[0m"
	fi

	# Power Settings
	if [ -f "/usr/share/applications/gnome-power-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-power-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-power-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Power Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Power Settings\033[0m"
	fi

	# Privacy Settings
	if [ -f "/usr/share/applications/gnome-privacy-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-privacy-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-privacy-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Privacy Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Privacy Settings\033[0m"
	fi

	# Region + Language Settings
	if [ -f "/usr/share/applications/gnome-region-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-region-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-region-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Region + Language Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Region + Language Settings\033[0m"
	fi

	# Removable Media Settings
	if [ -f "/usr/share/applications/gnome-removable-media-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-removable-media-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-removable-media-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Removable Media Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Removable Media Settings\033[0m"
	fi

	# Screensaver Settings
	if [ -f "/usr/share/applications/gnome-screensaver.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-screensaver.desktop
		sudo chmod 700 /usr/share/applications/gnome-screensaver.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Screensaver Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Screensaver Settings\033[0m"
	fi

	# Universal Access Settings
	if [ -f "/usr/share/applications/gnome-universal-access-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-universal-access-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-universal-access-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Universal Access Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Universal Settings\033[0m"
	fi

	# User Account Settings
	if [ -f "/usr/share/applications/gnome-user-accounts-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-user-accounts-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-user-accounts-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted User Account Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict User Account Settings\033[0m"
	fi

	# Wacom Tablet Settings
	if [ -f "/usr/share/applications/gnome-wacom-panel.desktop" ]; then
		sudo chown staff:staff /usr/share/applications/gnome-wacom-panel.desktop
		sudo chmod 700 /usr/share/applications/gnome-wacom-panel.desktop
		echo -e "\033[1;32m\xE2\x9C\x94 Restricted Wacom Tablet Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Wacom Tablet Settings\033[0m"
	fi

	# Budgie Desktop Settings
	if [ -f "/usr/share/applications/budgie-desktop-settings.desktop" ]; then
  	sudo chown staff:staff /usr/share/applications/budgie-desktop-settings.desktop
  	sudo chmod 700 /usr/share/applications/budgie-desktop-settings.desktop
  	sudo mv /usr/share/applications/budgie-desktop-settings.desktop /home/staff/.local/share/applications/budgie-desktop-settings.desktop
	  echo -e "\033[1;32m\xE2\x9C\x94 Restricted Budgie Desktop Settings"
  elif [ -f "/home/staff/.local/share/applications/budgie-desktop-settings.desktop" ]; then
    sudo chown staff:staff /home/staff/.local/share/applications/budgie-desktop-settings.desktop
    sudo chmod 700 /home/staff/.local/share/applications/budgie-desktop-settings.desktop
  	echo -e "\033[1;32m\xE2\x9C\x94 Restricted Budgie Desktop Settings"
	else
			echo -e "\033[0;31m\u274c Failed to Restrict Budgie Desktop Settings\033[0m"
	fi
}

function De_Restrict() {
        echo
	# Background Picture
	sudo chown staff:staff /usr/share/applications/gnome-background-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-background-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Background Picture Settings"

	# Date + Time
	sudo chown staff:staff /usr/share/applications/gnome-datetime-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-datetime-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Date + Time Settings"

	# Default Apps
	sudo chown staff:staff /usr/share/applications/gnome-default-apps-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-default-apps-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Default App Settings"

	# Display Configurations
	sudo chown staff:staff /usr/share/applications/gnome-display-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-display-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Display Settings"

	# Keyboard Configurations
	sudo chown staff:staff /usr/share/applications/gnome-keyboard-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-keyboard-panel.desktop
	sudo chown staff:staff /usr/share/applications/gkbd-keyboard-display.desktop
	sudo chmod 755 /usr/share/applications/gkbd-keyboard-display.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Keyboard Settings"

	# Mouse Configurations
	sudo chown staff:staff /usr/share/applications/gnome-mouse-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-mouse-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Mouse Settings"

	# Notifications
	sudo chown staff:staff /usr/share/applications/gnome-notifications-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-notifications-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Notification Settings"

	# Online Account Integration
	sudo chown staff:staff /usr/share/applications/gnome-online-accounts-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-online-accounts-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Online Account Integration Settings"

	# Power Settings
	sudo chown staff:staff /usr/share/applications/gnome-power-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-power-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Power Settings"

	# Privacy Settings
	sudo chown staff:staff /usr/share/applications/gnome-privacy-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-privacy-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Privacy Settings"

	# Region + Language Settings
	sudo chown staff:staff /usr/share/applications/gnome-region-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-region-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Region + Language Settings"

	# Removable Media Settings
	sudo chown staff:staff /usr/share/applications/gnome-removable-media-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-removable-media-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Removable Media Settings"

	# Screensaver Settings
	sudo chown staff:staff /usr/share/applications/gnome-screensaver.desktop
	sudo chmod 755 /usr/share/applications/gnome-screensaver.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Screensaver Settings"

	# Universal Access Settings
	sudo chown staff:staff /usr/share/applications/gnome-universal-access-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-universal-access-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Universal Access Settings"

	# User Account Settings
	sudo chown staff:staff /usr/share/applications/gnome-user-accounts-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-user-accounts-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted User Account Settings"

	# Wacom Tablet Settings
	sudo chown staff:staff /usr/share/applications/gnome-wacom-panel.desktop
	sudo chmod 755 /usr/share/applications/gnome-wacom-panel.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Wacom Tablet Settings"

	# Budgie Desktop Settings
  if [ -f /home/staff/.local/share/applications/budgie-desktop-settings.desktop ]; then
	  sudo mv /home/staff/.local/share/applications/budgie-desktop-settings.desktop /usr/share/applications/budgie-desktop-settings.desktop
  fi
	sudo chown staff:staff /usr/share/applications/budgie-desktop-settings.desktop
	sudo chmod 755 /usr/share/applications/budgie-desktop-settings.desktop
	echo -e "\033[1;32m\xE2\x9C\x94 Reverted Budgie Desktop Settings"
}

function Incorrect() {
	echo
	echo "     ___________________________________________________________"
	echo -e "     | \033[1;34m Sadly, option $REPLY is not possible!     \033[0m                  |"
	echo -e "     | \033[1;34m Please select either option 1, 2, 3, 4, 5 or 6 instead.\033[0m|"
	echo -e "     | \033[1;34m Thank you! \033[0m                                            |"
	echo "     -----------------------------------------------------------"
	echo
}

function Power_Strip() {
  # Get the Real Username
  RUID=$(who | awk 'FNR == 1 {print $1}')

  # Translate Real Username to Real User ID
  RUSER_UID=$(id -u ${RUID})

  # Set gsettings for the Real User
  sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /com/solus-project/budgie-raven/show-power-strip true
	sudo -u patron HOME=/home/patron dbus-launch --exit-with-session dconf write /com/solus-project/budgie-raven/show-power-strip true >/dev/null 2>/dev/null
 	echo -e "\033[1;32m\xE2\x9C\x94 Added Power Strip back for all users"
}

function LibreOffice() {
	echo
	libreoffice --safe-mode >/dev/null 2>/dev/null
 	echo -e "\033[1;32m\xE2\x9C\x94 LibreOffice should be fixed"
}

function Maintenance() {
	echo
	echo -e "\033[1;33m            Optimizing Package Manager... \033[0m"
	echo
	sudo eopkg clean --no-color
	sudo eopkg delete-cache --no-color
	sudo eopkg -y rebuild-db --no-color
	sudo eopkg update-repo --no-color
	echo
 	echo -e "\033[1;32m\xE2\x9C\x94 Package Manager has been optimized"
	echo -e "\033[1;33m            Updating all possible packages... \033[0m"
	echo
	sudo eopkg upgrade --no-color -y
	echo
 	echo -e "\033[1;32m\xE2\x9C\x94 All possible packages have been updated"
	echo -e "\033[1;33m            Checking for broken packages..."
	if (( $(sudo eopkg check | grep -c Broken) != 0)); then
		echo -e "\033[1;31m Broken packages found..."
		echo
		sudo eopkg check | grep Broken | awk '{print $4}' | xargs sudo eopkg it --reinstall
		echo
	fi
 	echo -e "\033[1;32m\xE2\x9C\x94 All packages are good to go"
	echo -e "\033[1;33m            Updating Boot Manager..."
	sudo clr-boot-manager update
 	echo -e "\033[1;32m\xE2\x9C\x94 Boot Manager is updated"
	echo -e "\033[1;33m            Correcting Firefox Settings..."
	if [[ ! -f /tmp/prefs.js || $(date +%d) != 01 ]]; then
		sudo cp -f /home/staff/.mozilla/firefox/$FIREFOX_SETTINGS/prefs.js /tmp/
		sudo chmod 755 /tmp/prefs.js
	fi
	sudo cp -f /tmp/prefs.js /home/staff/.mozilla/firefox/$FIREFOX_SETTINGS/
	echo $(sudo -u patron ls /home/patron/.mozilla/firefox/) >/tmp/try.txt
	FIREFOX_SETTINGS2=$(cat /tmp/try.txt | tr ' ' '\n' | grep default)
	sudo cp -f /tmp/prefs.js /home/patron/.mozilla/firefox/$FIREFOX_SETTINGS2/
	sudo rm -f /tmp/try.txt
 	echo -e "\033[1;32m\xE2\x9C\x94 Firefox settings are properly configured for all users"
	echo -e "\033[1;33m            Ensuring proper window configs..."
	dconf write /org/gnome/desktop/wm/preferences/button-layout "'appmenu:minimize,maximize,close'"
	dconf write /com/solus-project/budgie-wm/button-layout "'appmenu:minimize,maximize,close'"
	sudo -u patron HOME=/home/patron dbus-launch --exit-with-session dconf write /org/gnome/desktop/wm/preferences/button-layout "'appmenu:minimize,maximize,close'" >/dev/null 2>/dev/null
	sudo -u patron HOME=/home/patron dbus-launch --exit-with-session dconf write /com/solus-project/budgie-wm/button-layout "'appmenu:minimize,maximize,close'" >/dev/null 2>/dev/null
 	echo -e "\033[1;32m\xE2\x9C\x94 Ensured proper window settings are in place"
	echo -e "\033[1;33m            Ensuring Power Strip is visible..."
	dconf write /com/solus-project/budgie-raven/show-power-strip true
	sudo -u patron HOME=/home/patron dbus-launch --exit-with-session dconf write /com/solus-project/budgie-raven/show-power-strip true >/dev/null 2>/dev/null
 	echo -e "\033[1;32m\xE2\x9C\x94 Ensured Power Strip is visible for all users"
}

function Revert() {
	echo
	echo "VIEW SOLUS UPDATE HISTORY"
	echo
	sudo eopkg history | head -50
}

function Printers() {
  gnome-control-center printers
}

function Firefox() {
  FIREFOX_SETTINGS=$(cd /home/staff/.mozilla/firefox/ && ls | grep "default")
  echo -e "\033[1;33m            Correcting Firefox Settings..."
  if [[ ! -f /tmp/prefs.js || $(date +%d) != 01 ]]; then
    sudo cp -f /home/staff/.mozilla/firefox/$FIREFOX_SETTINGS/prefs.js /tmp/
    sudo chmod 755 /tmp/prefs.js
  fi
  sudo cp -f /tmp/prefs.js /home/staff/.mozilla/firefox/$FIREFOX_SETTINGS/
  echo $(sudo -u patron ls /home/patron/.mozilla/firefox/) >/tmp/try.txt
  FIREFOX_SETTINGS2=$(cat /tmp/try.txt | tr ' ' '\n' | grep default)
  sudo cp -f /tmp/prefs.js /home/patron/.mozilla/firefox/$FIREFOX_SETTINGS2/
  sudo rm -f /tmp/try.txt
  echo -e "\033[1;32m\xE2\x9C\x94 Firefox settings are properly configured for all users"
}

function DesktopIcons {
  # Get the Real Username
  RUID=$(who | awk 'FNR == 1 {print $1}')

  # Translate Real Username to Real User ID
  RUSER_UID=$(id -u ${RUID})

  status=$(sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf read /org/gnome/desktop/background/show-desktop-icons)

  if [ "$status" == "true" ]; then
    sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /org/gnome/desktop/background/show-desktop-icons false
    sudo -u patron HOME=/home/patron dbus-launch --exit-with-session dconf write /org/gnome/desktop/background/show-desktop-icons false
  else
    sudo -u ${RUID} DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" dconf write /org/gnome/desktop/background/show-desktop-icons true
    sudo -u patron HOME=/home/patron dbus-launch --exit-with-session dconf write /org/gnome/desktop/background/show-desktop-icons true
  fi
  echo -e "\033[1;32m\xE2\x9C\x94 The status of icons on the desktop has been successfully changed"
}

function Wallpapers {
  if [ -d '/wallpapers/' ]; then
  	echo "Good to go!"
  else
  	sudo mkdir /wallpapers
  	sudo chmod 777 /wallpapers
  	sudo chown staff:staff /wallpapers
  	sudo cp /usr/share/CCRLD/wallpaper.desktop /etc/xdg/autostart/
  	sudo chmod 777 /etc/xdg/autostart/wallpaper.desktop
  	sudo chown staff:staff /etc/xdg/autostart/wallpaper.desktop
  fi
  if [[ $# -gt 0 ]]; then
    sudo rm -rf /wallpapers/*
    sudo cp "$1" /wallpapers/
    for i in /wallpapers/*; do
      sudo chmod 777 "$i"
      echo "$i will be the new desktop wallpaper"
      # Get the Real Username
      RUID=$(who | awk 'FNR == 1 {print $1}')

      # Translate Real Username to Real User ID
      RUSER_UID=$(id -u ${RUID})
    	sudo -u $RUID DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${RUSER_UID}/bus" gsettings set org.gnome.desktop.background picture-uri "file://$i"
    done
  else
    for i in /wallpapers/*; do
    	echo "$i will be the new desktop wallpaper"
    	gsettings set org.gnome.desktop.background picture-uri "file://$i"
    done
  fi
}

function SyncStatus {
  if [ -d /sync ]; then
    status="Current Status: ENABLED"
  else
    status="Current Status: DISABLED"
  fi
  echo "$status" > $1/sync_status.txt
}

function Sync {
  sudo /usr/share/CCRLD/sync/sync.sh Enable
}

function UnSync {
  sudo /usr/share/CCRLD/sync/sync.sh Disable
}

function SearchUpdates {
  if [[ ! -f /usr/share/CCRLD/updates/current_version.txt ]]; then
      mkdir /usr/share/CCRLD/updates
      echo "1" >/usr/share/CCRLD/updates/current_version.txt
  fi
  wget -q --spider http://google.com
  if [ $? -eq 0 ]; then
    RUID=$(who | awk 'FNR == 1 {print $1}')
    latest_version=$(git ls-remote --tags https://gitlab.com/keeganmilsten/ccrld.git | grep -v "{" | awk '{print $2}' | tail -1)
    version_number=${latest_version##*/}
    version_digit=${version_number%.*}
    git clone https://gitlab.com/keeganmilsten/ccrld.git --branch $version_number --depth 1 /home/$RUID/CCRLD
    cd /home/$RUID/CCRLD
    comments=$(git tag -l -n99 $version_number | tr -d $version_number | sed 's/^ *//' | sed 's/^/\  \  /')

    if (( "$(head -n 1 /usr/share/CCRLD/updates/current_version.txt)" != "$version_digit" )); then
        install_updates=$(yad --width=500 --title=CCRLD --on-top --center --text "<b><big><big> An update is available. Would you like to install it?</big></big></b>\n\n This update includes the following features: \n $comments" --button=No:1 --button=Yes:0)
        ret=$?
        if [[ $ret -eq 0 ]]; then
            git clone https://gitlab.com/keeganmilsten/ccrld.git --branch $version_number --depth 1 /home/$RUID/CCRLD
            cd /home/$RUID/CCRLD/
            echo $version_digit >/usr/share/CCRLD/updates/current_version.txt
            bash ./install.sh
        fi
    fi
    cd ${0%/*}
    rm -rf /home/$RUID/CCRLD
  fi
}

function Install {
  cd ${0%/*}
  RUID=$(who | awk 'FNR == 1 {print $1}')
  yes | sudo eopkg install yad
  yes | sudo eopkg install git
  sudo mkdir /usr/share/CCRLD
  sudo touch /usr/share/CCRLD/updates.txt
  sudo chown -R staff:staff /usr/share/CCRLD
  sudo chmod 755 /usr/share/CCRLD
  sudo cp -f ./wallpaper.desktop /usr/share/CCRLD/
  sudo chmod 777 /usr/share/CCRLD/wallpaper.desktop
  sudo chown staff:staff /usr/share/CCRLD/wallpaper.desktop
  sudo cp -f ./library-settings.sh /usr/share/CCRLD/
  sudo chmod 777 /usr/share/CCRLD/library-settings.sh
  sudo chown staff:staff /usr/share/CCRLD/library-settings.sh
  sudo cp -f ./library.py /usr/share/CCRLD/
  sudo chmod 777 /usr/share/CCRLD/library.py
  sudo chown staff:staff /usr/share/CCRLD/library.py
  sudo cp -f ./library.glade /usr/share/CCRLD/
  sudo chmod 777 /usr/share/CCRLD/library.glade
  sudo chown staff:staff /usr/share/CCRLD/library.glade
  sudo cp -f ./CCRLD.desktop /usr/share/applications/
  sudo chmod 777 /usr/share/applications/CCRLD.desktop
  sudo chown staff:staff /usr/share/applications/CCRLD.desktop
  sudo cp -rf ./sync /usr/share/CCRLD/
  sudo find /usr/share/CCRLD -type d -exec chmod 770 {} \;
  sudo find /usr/share/CCRLD -type f -exec chmod 770 {} \;
  sudo find /usr/share/CCRLD -type d -exec chown staff:staff {} \;
  sudo find /usr/share/CCRLD -type f -exec chown staff:staff {} \;
  yad --width=300 --title=CCRLD --on-top --center --text-align=center --text "<b><big>Application is installed</big></b>" --button=OK:1
  sudo rm -rf /home/$RUID/CCRLD
}

export -f Restrict De_Restrict Incorrect Power_Strip Maintenance LibreOffice Revert Printers Firefox DesktopIcons Wallpapers SyncStatus Sync UnSync SearchUpdates Install

"$@"
