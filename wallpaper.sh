directory=$(pwd)

if [ -d '/wallpapers/' ]; then
	echo "Good to go!"
else
	sudo mkdir /wallpapers
	sudo chmod 777 /wallpapers
	sudo chown staff:staff /wallpapers
	sudo cp $directory/"CCRLD June Events 2019.png" /wallpapers/
	sudo cp $directory/wallpaper.desktop /etc/xdg/autostart/
	sudo chmod 777 /etc/xdg/autostart/wallpaper.desktop
	sudo chown staff:staff /etc/xdg/autostart/wallpaper.desktop
	sudo cp $directory/wallpaper.sh /usr/bin/
	sudo chmod 775 /usr/bin/wallpaper.sh
	sudo chown staff:staff /usr/bin/wallpaper.sh
	sudo cp $directory/wallpapers-gui.sh /usr/bin/
	sudo chmod 777 /usr/bin/wallpapers-gui.sh
	sudo chown staff:staff /usr/bin/wallpapers-gui.sh
	sudo cp $directory/wallpapers-gui.desktop /usr/share/applications/
	sudo chmod 777 /usr/share/applications/wallpapers-gui.desktop
	sudo chown staff:staff /usr/share/applications/wallpapers-gui.desktop
	sudo eopkg install yad
fi
cd /wallpapers/
for i in *; do
	echo "$i will be the new desktop wallpaper"
	gsettings set org.gnome.desktop.background picture-uri "file:///wallpapers/$i"
done
