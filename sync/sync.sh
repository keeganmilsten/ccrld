#/bin/bash
################################################################################
############ SYNC PATRON DIRECTORIES TO WIPE ALL CHANGES BY PATRONS ############
################################################################################

cd /usr/share/CCRLD/sync/

function Enable {
	if [[ ! -d /sync/.patron || ! -d /sync/.patron-sync || ! -f /etc/xdg/autostart/sync.desktop || ! -f /sync/.patron-sync/sync.sh || ! -f /usr/bin/userlog.sh || ! -f /tmp/userlog.txt ]]; then
		# Make /sync directory and append the proper permissions for it
		sudo mkdir /sync
		sudo chown staff:staff /sync
		sudo chmod 111 /sync

		# Make /sync/.patron directory and append the proper permissions for it.
		# Not only is this directory hidden by default from users, it also CANNOT even be accessed by anyone other than root
		sudo mkdir /sync/.patron
		sudo chown patron:patron /sync/.patron
		sudo chmod -R 777 /sync/.patron

		# Make /sync/.patron-sync directory and append the proper permissions for it
		# This directory cannot be written to, in order to ensure its security.
		# However, it must be able to be read and executed, as sync.sh will be hidden inside.
		sudo mkdir /sync/.patron-sync
		sudo chown staff:staff /sync/.patron-sync
		sudo chmod 777 /sync/.patron-sync

		# Copy over sync.sh to be run upon login, located within the protected /sync/.patron-sync directory.
		# This script will be run as root, triggered by the userlog.sh file shown below.
		sudo cp -f ./sync.sh /sync/.patron-sync/
		sudo chown staff:staff /sync/.patron-sync/sync.sh
		sudo chmod 555 /sync/.patron-sync/sync.sh

		# Copy over sync.desktop to be run upon login.
		# sync.desktop will launch the userlog.sh script shown below
		sudo cp -f ./sync.desktop /etc/xdg/autostart/sync.desktop
		sudo chown staff:staff /etc/xdg/autostart/sync.desktop
		sudo chmod 555 /etc/xdg/autostart/sync.desktop

		# Copy over userlog.sh to /usr/bin/ to be run by sync.desktop upon login
		# This script will log the name of the current user into /tmp/userlog.txt for sync.sh to later grab as a value
		sudo cp -f ./userlog.sh /usr/bin/
		sudo chown patron:patron /usr/bin/userlog.sh
		sudo chmod 777 /usr/bin/userlog.sh

		# In essence, sync.desktop will trigger userlog.sh as a normal user so it can write the username to a file.
		# Once that is done, userlog.sh will trigger sync.sh to run as root and perform the necessary commands
		# based on the detected user - as displayed within the /tmp/userlog.txt file.

		# To ensure these scripts can be run without a password prompt, an exception for them
		# is necessary in the /etc/sudoers file. Due to how protected these files are, this process
		# should be entirely safe - at the very least, much safer than the previous method used before this.
		# Not only is sync.sh unable to be read at all by ANYONE other than root, it is buried under directories
		# that do NOT include an executable permission in them as well. To top that all off, the mirror directory /sync/.patron
		# is protected from reading, writing, AND executing - maximizing the security for it as well.
		# Even is the user figured out these scripts could run without root prompting, they could not read the scripts,
		# nor could they edit them in any way. Plus, they would need the necessary passwords to even open the scripts at all!

		# Get the Real Username
		RUID=$(who | awk 'FNR == 1 {print $1}')

		sudo echo "$RUID" >/tmp/userlog.txt
		sudo chown patron:patron /tmp/userlog.txt
		sudo chmod 777 /tmp/userlog.txt
		rsync -azh --delete --exclude=".*" --info=progress /home/patron/ /sync/.patron/
		exit
	fi

	# If the current user is "patron", rsync the contents from the mirror directory (/sync/.patron)
	# to /home/patron/ so as to eliminate all unnecessary content from it.
	if [[ $(sed -n '1p' /tmp/userlog.txt) == "patron" ]]; then
		rsync -azh --delete --exclude=".*" --info=progress /sync/.patron/ /home/patron/
	fi
}

function Disable {
	while [[ -d /sync ]]; do
		sudo rm -rf /sync
	done
	sudo rm -f /etc/xdg/autostart/sync.desktop
	sudo rm -f /usr/bin/userlog.sh
	sudo rm -f /tmp/userlog.txt
}

export -f Enable Disable
"$@"
